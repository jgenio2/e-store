import * as React from 'react';
import { useEffect, useState } from 'react';
import './App.scss';
import TopBar from './components/TopBar/TopBar';
import ProductList from './components/ProductList/ProductList';
import { callApi, services } from './services';
import { Product } from './types/types'

const App = () => {
  const [layout, setLayout] = useState({ categories: []});
  const [productsList, setProductsList] = useState<Product[]>([]);

  useEffect(() => {
    async function loadLayout() {
      const response = await callApi(services.getLayout());
      setLayout(response.data);
    }
    loadLayout();
  }, []);

  function loadProductsOnPage(products: React.SetStateAction<Product[]>) {
    setProductsList(products);
  }
  
  return (
    <div className="App">
      <TopBar layout={layout} onItemsListFetched={loadProductsOnPage} />
      <ProductList items={productsList} />
    </div>
  );
}

export default App;
