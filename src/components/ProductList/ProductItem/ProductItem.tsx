import * as React from 'react'
import { FunctionComponent } from 'react';
import './ProductItem.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'
import { Product } from '../../../types/types';

interface ProductItemProps {
  data: Product
}

const ProductItem: FunctionComponent<ProductItemProps> = ({ data }) => {
  const itemData = data;

  function getDecimalPart(num: number): number {
    if (Number.isInteger(num)) {
      return 0;
    }
  
    const decimalStr = num.toString().split('.')[1];
    return Number(decimalStr);
  }

  const priceTemplate = (<span className="price">
      <small className="price-euro">€</small>
      { Math.trunc(itemData.price) },
      <sup>{ getDecimalPart(itemData.price) }</sup>
    </span>);

  return (
    <div className="ProductItem col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
      <div className="ProductItemContent">
        <img alt={itemData.name} className="image" src={itemData.image} />
        <span className="name">{itemData.name}</span>
        { priceTemplate }

        <button type="button" title="Add" className="btn-add-cart">
          <FontAwesomeIcon icon={faCartPlus} />
          Add
        </button>
      </div>
    </div>
  );
}



export default ProductItem;
