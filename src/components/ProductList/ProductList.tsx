import * as React from 'react'
import { FunctionComponent } from 'react';
import './ProductList.scss';
import ProductItem from './ProductItem/ProductItem';
import { Product } from '../../types/types';

interface ProductListProps {
  items: Product[]
}

const ProductList: FunctionComponent<ProductListProps> = ({ items }) => {

  const itemsLayout = items?.map(item => 
    <ProductItem key={item.id} data={item}/>
  );

  return (
    <div className="ProductList container">
      <div className="row">
        { itemsLayout }
      </div>
    </div>
  );
}



export default ProductList;
