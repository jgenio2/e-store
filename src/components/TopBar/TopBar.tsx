import * as React from 'react';
import { useState, FunctionComponent } from 'react';
import './TopBar.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import { callApi, services } from '../../services';
import { Product, Category } from '../../types/types';

interface TopBarProps {
  layout: { categories: Category[]; },
  onItemsListFetched(products: Product[]): void
}

const TopBar: FunctionComponent<TopBarProps> = ({ layout, onItemsListFetched }) => {
  const [isMenuVisible, setIsMenuVisible] = useState(false);
  const [menuLayout, setMenuLayout] = useState('');

  const menuNavLinks = layout?.categories?.map(category =>
    <div className="nav-link" data-key={category.id} key={category.id} onMouseEnter={handleOnMouseEnter} onClick={handleItemClick}>
      {category.label}
      <FontAwesomeIcon icon={faChevronDown} />
    </div>
  );

  async function handleItemClick(evt: React.MouseEvent<HTMLLIElement | HTMLDivElement, MouseEvent>): Promise<void> {
    const selectedCategory = evt.currentTarget.getAttribute('data-key');
    const productListResponse = await callApi(services.getProductsList(selectedCategory));

    onItemsListFetched(productListResponse.data.products);
  }

  function handleOnMouseEnter(evt: React.MouseEvent<HTMLDivElement, MouseEvent>): void {
    const key = evt.currentTarget.getAttribute('data-key');
    const mainCategories = layout.categories;
    const selectedMainCategory = mainCategories.find(section => section.id===key);

    if(selectedMainCategory){
      setMenuLayout(selectedMainCategory.categories.map(category =>
        <div className="category" key={category.id}>
          <span data-key={category.id} onClick={handleItemClick}>{category.label}</span>
          <ul>
            {category.categories?.map(subCategory =>
              <li data-key={subCategory.id} key={subCategory.id} onClick={handleItemClick}>
                {subCategory.label}
              </li>
            )}
          </ul>
        </div>
      ));
      setIsMenuVisible(true);
    }
  }

  function handleOnMouseLeave(evt) {
    setMenuLayout('');
    setIsMenuVisible(false);
  }

  return (
    <div className="TopBar" onMouseLeave={handleOnMouseLeave}>
      <nav className="navigation-bar">
        { menuNavLinks }
      </nav>
      <div className={`TopBarContent ${isMenuVisible ? '' : 'hidden'}`}>
        { menuLayout }
      </div>
    </div>
  );
}



export default TopBar;
