import { rest } from 'msw'
import layout from "./responses/layout.json";
import productsList from "./responses/productsList.json";

const baseUrl = 'estore';
export const handlers = [
  // Capture a GET /user/:userId request,
  rest.get(baseUrl + '/layout', (req, res, ctx) => {
    // ...and respond with this mocked response.
    return res(ctx.json(layout));
  }),

  rest.get(baseUrl + '/productsList', (req, res, ctx) => {
    // ...and respond with this mocked response.
    return res(ctx.json(productsList));
  }),
]