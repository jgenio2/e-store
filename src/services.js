const axios = require('axios');

const baseUrl = 'estore';

export const services = {
    getLayout: () =>({
        url: baseUrl + '/layout',
        method: 'get',
    }),
    getProductsList: (category) =>({
        url: baseUrl + '/productsList',
        method: 'get',
        params: {
            category,
        },
    }),
};

export const callApi = async(service) => {
    try {
        const response = await axios(service);
        return response;
      } catch (error) {
        console.error(error);
      }
}
    