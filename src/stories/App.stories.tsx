import * as React from 'react'

import App from '../App';

const { worker } = require('../mocks/browser')
worker.start()

export default {
  title: 'Example/App',
  component: App,
};

export const AppSample = () => (
  <App />
);
