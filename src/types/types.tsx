export type Product = {
    id: number,
    name: string,
    image: string,
    price: number
}
  
export type Category = {
    map(category: (category: Category) => JSX.Element): React.SetStateAction<string>;
    id: string,
    label: string,
    categories: Category
}