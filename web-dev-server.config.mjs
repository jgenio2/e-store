import { storybookPlugin } from '@web/dev-server-storybook';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  // type can be 'web-components' or 'preact'
  plugins: [storybookPlugin({ type: 'web-components' })],
};